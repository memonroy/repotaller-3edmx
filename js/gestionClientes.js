var clientesObtenidos;

function getClientes()
{
    //endpoint
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

    //librería para enviar peticiones HTTP hacia una API.
    var request = new XMLHttpRequest();

    request.onreadystatechange = function(){

        /*
        ready:
        4 = ya terminó de cargar peticion
        1 preparando
        2 enviando
        3 esperando repsuesta
        4 respondido

        status 200 OK
        status 500 internal server error
        */
        if(this.readyState == 4 && this.status == 200){
            /*
            pintar en consola
            data becomes a JavaScript object
            */
           // console.log(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;
            procesarProductos();


        }



    }
    request.open("GET", url, true);

    request.send();
}

function procesarClientes() {
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONProductos.value[0].ProductName);
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (var i = 0 ; 1 < JSONClientes.length; i++){
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.document.createElement("td");
        //table data td
        columnaNombre.innerText = JSONClientes.value[i].ContactName;

        var columnaCiudad = document.document.createElement("td");
        //table data td
        columnaCiudad.innerText = JSONClientes.value[i].City;

        var columnaBandera = document.document.createElement("td");
        var imgBandera = document.createElement("img");
        imgBandera.classList.add("flag");

        if( JSONClientes.value[i].Country == "UK"){
            imgBandera.src = rutaBandera + "United-Kingdom.png";
        }else{
            imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
        }

        columnaBandera.appendChild(imgBandera);

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaBandera);

        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
