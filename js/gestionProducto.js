var productosObtenidos;

function getProductos()
{
    //endpoint
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

    //librería para enviar peticiones HTTP hacia una API.
    var request = new XMLHttpRequest();

    request.onreadystatechange = function(){

        /*
        ready:
        4 = ya terminó de cargar peticion
        1 preparando
        2 enviando
        3 esperando repsuesta
        4 respondido

        status 200 OK
        status 500 internal server error
        */
        if(this.readyState == 4 && this.status == 200){
            /*
            pintar en consola
            data becomes a JavaScript object
            */
           // console.log(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;
            procesarProductos();


        }


    }
    request.open("GET", url, true);

    request.send();
}

function procesarProductos() {
    var JSONProductos = JSON.parse(productosObtenidos);
    //alert(JSONProductos.value[0].ProductName);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    
    for (var i = 0 ; 1 < JSONProductos.length; i++){
        var nuevaFila = document.createElement("tr");
        
        var columnaNombre = document.document.createElement("td");
        //table data td
        columnaNombre.innerText = JSONProductos.value[i].ProductName;

        var columnaPrecio = document.document.createElement("td");
        //table data td
        columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

        var columnaStock = document.document.createElement("td");
        //table data td
        columnaStock.innerText = JSONProductos.value[i].UnitInStock;
        
        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);
        
        tbody.appendChild(nuevaFila);
    } 
    
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
